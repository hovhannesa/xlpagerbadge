import Foundation
import UIKit

class UIUtils {
    
    static func getCollectionViewLayout(cellCount: CGFloat,
                                        insetsTop top: CGFloat = 5.0,
                                        insetsLeft left: CGFloat = 5.0,
                                        insetsBottom bottom: CGFloat = 5.0,
                                        insetsRight right: CGFloat = 5.0,
                                        minimumInteritemSpacing mis: CGFloat = 1.0,
                                        minimumLineSpacing mls: CGFloat = 7.0) -> UICollectionViewFlowLayout {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let width = UIScreen.main.bounds.width
        layout.sectionInset = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        let size = ((width - 25) / cellCount)
        layout.itemSize = CGSize(width: size, height: size)
        layout.minimumInteritemSpacing = mis
        layout.minimumLineSpacing = mls
        
        return layout
    }
}
