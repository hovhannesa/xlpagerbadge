import UIKit
import XLPagerTabStrip

class ViewController: BaseButtonBarPagerTabStripViewController<UnreadMessagesTitleCell> {

    let tintColor = UIColor(hexString: "009FE3")
    let redColor = UIColor(red: 221/255.0, green: 0/255.0, blue: 19/255.0, alpha: 1.0)
    let unselectedIconColor = UIColor(red: 73/255.0, green: 8/255.0, blue: 10/255.0, alpha: 1.0)
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        buttonBarItemSpec = ButtonBarItemSpec.nibFile(nibName: "UnreadMessagesTitleCell", bundle: Bundle(for: UnreadMessagesTitleCell.self), width: { _ in
            return 95.0
        })
    }

    override func viewDidLoad() {
        self.buttonBarView!.collectionViewLayout = UIUtils.getCollectionViewLayout(cellCount: 2)
        settings.style.buttonBarBackgroundColor = tintColor
        settings.style.buttonBarItemBackgroundColor = tintColor
        settings.style.selectedBarBackgroundColor = .gray
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 10
        settings.style.buttonBarItemTitleColor = .black
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: UnreadMessagesTitleCell?, newCell: UnreadMessagesTitleCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.lblTitle.textColor = .black
            newCell?.lblTitle.textColor = .white
            
            if (newCell?.lblTitle.text == "Child1") {
                var count = Int(newCell?.lblUnreadMsgCount.text ?? "0")
                
                if (count == 5) {
                    count = 0
                }
                newCell?.lblUnreadMsgCount.text = String(count! + 1)
            }
        }
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func configure(cell: UnreadMessagesTitleCell, for indicatorInfo: IndicatorInfo) {
        print(indicatorInfo)
        cell.lblTitle.text = indicatorInfo.title
//        cell.iconImage.image = indicatorInfo.image?.withRenderingMode(.alwaysTemplate)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child1")
        let child_2 = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "child2")
        return [child_1, child_2, child_1]
    }
}

