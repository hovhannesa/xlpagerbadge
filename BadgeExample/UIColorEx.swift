import Foundation
import UIKit

extension UIColor {

    public convenience init(rgb: Int) {
        self.init(
            red:   CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8)  / 255.0,
            blue:  CGFloat((rgb & 0x0000FF) >> 0)  / 255.0,
            alpha: 1)
    }
    
    public convenience init(hexString: String) {
        var colorInt: UInt32 = 0
        Scanner(string: hexString).scanHexInt32(&colorInt)
        self.init(rgb: (Int) (colorInt ?? 0xaaaaaa))
    }
}
