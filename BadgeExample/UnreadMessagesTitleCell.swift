import UIKit

class UnreadMessagesTitleCell: UICollectionViewCell {
    @IBOutlet weak var lblUnreadMsgCount: BadgeLabel!
    
    @IBOutlet weak var lblTitle: UILabel!
}
